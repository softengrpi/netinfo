#!/usr/bin/env bash

MONITOR_PROG="monitor.out"  # Program that monitor the cpu usage
TEST_TIME=900               # How long should we run the test for? (seconds)

############################################################################
###########################     DO NOT TOUCH     ###########################
############################################################################
NTP_SERVER=                 # Local NTP server used to sync hour 
RAMDISK=''                  # Location where we mount the ramdisk
IPERF_BIND_IFACE=''         # The IP address which we want to bind iPerf client to
IPERF_CONNECT_TO_IFACE=''   # The IP address which the iPerf client will connect to
CUR_DIR=$(pwd)              # The current working directory of the script
SAMPLING_START=''           # Start Epoch time of sampling with iPerf

function print_help(){
    case $1 in
        "args")
            echo
            echo "$0 <psw> <ntp_server_address> <iface> <connect_to>"
            echo
            echo "Where:"
            echo "  - <psw>: password for super user"
            echo "  - <ntp_server_address>: IPv4 address of the local NTP server used to sync the time"
            echo "  - <iface>: Network interface to sample"
            echo "  - <connect_to>: IP address of the iPerf server"
            echo
            ;;
        "monitor")
            echo
            echo "Monitor program $MONITOR_PROG was not found"
            echo "Maybe you need to compile it?"
            echo
            ;;
        "ntpdate")
            echo
            echo "ntpdate was not found"
            echo "Maybe you need to install it?"
            echo
            ;;
        "iperf")
            echo
            echo "Loader program iperf3.1+ was not found"
            echo "Maybe you need to install it?"
            echo
            ;;
        *)
            echo
            echo "Something went terribly wrong"
            echo
            ;;
    esac
}


#Creates and mounts the ramdisk on which we'll save the results of the monitoring
function make_ramdisk(){
    RAMDISK="/tmp/ramdisk`date +%s`"
    mkdir $RAMDISK || return 1
    echo $1 | sudo -S mount -t ramfs -o size=128m ramfs $RAMDISK || return 1
    sudo chmod 777 $RAMDISK || return 1
    echo "Ramdisk mounted successfully at $RAMDISK"
    return 0
}

#Runs the test FROM CLIENT TO SERVER with a specified load.
#The function takes as input the bandwidth (Mb/s) we want to use for the test 
function run_c2s_test(){
    if [[ $1 == "" ]]; then
        echo
        echo "Please provide a bandwidth (Mb/s) as load for iPerf"
        echo
        return 1
    fi

    #Sync time just before starting
    echo $psw | sudo -S ntpdate -u $NTP_SERVER > /dev/null 2>&1|| { echo "Cannot find NTP server $NTP_SERVER"; return 1; }

    echo "C2S - Running test with bandwidth $1Mb/s"
    ./"$MONITOR_PROG" "$RAMDISK" "$iface" "$1" "c2s" &
    monitor_pid=$(ps aux | grep "$MONITOR_PROG" | head -n 1 | tr -s " " | cut -f 2 -d " ")

    #Start the monitor
    kill -s 10 $monitor_pid

    #Run tests
    SAMPLING_START=$(date +%s)
    iperf3 -f m -B "$IPERF_BIND_IFACE" -c "$IPERF_CONNECT_TO_IFACE" -u -b $1M -t $TEST_TIME --logfile "$RAMDISK/iperf_c2s_$1.txt"

    #Once the tests are over stop the monitor
    kill -s 12 $monitor_pid

    if mv $RAMDISK/* $CUR_DIR/ ; then
        echo "All files copied to $CUR_DIR"
        echo
        echo
        parse_iperf "c2s" $1 || { echo "An error occured in parse_iperf function"; return 1; }
        return 0
    else
        echo "Copy of files failed, do it yourself."
        echo "iPerf results won't be in csv format and won't have an epoch time"
        echo "You can obtain the CSV file by looking at parse_iperf function, but you won't be able to get an epoch for it."
        echo "Files are located at $RAMDISK"
        echo
        echo
        return 1
    fi
}

function run_s2c_test(){
    if [[ $1 == "" ]]; then
        echo
        echo "Please provide a bandwidth (Mb/s) as load for iPerf"
        echo
        return 1
    fi

    #Sync time just before starting
    echo $psw | sudo -S ntpdate -u $NTP_SERVER > /dev/null 2>&1|| { echo "Cannot find NTP server $NTP_SERVER"; return 1; }

    echo "S2C - Running test with bandwidth $1Mb/s"
    ./"$MONITOR_PROG" "$RAMDISK" "$iface" "$1" "s2c" &
    # Sleep a bit, monitor may not be ready, we won't find the pid otherwise
    monitor_pid=$(ps aux | grep "$MONITOR_PROG" | head -n 1 | tr -s " " | cut -f 2 -d " ")

    #Start the monitor
    kill -s 10 $monitor_pid

    SAMPLING_START=$(date +%s)
    #Run tests
    iperf3 -f m -R -B "$IPERF_BIND_IFACE" -c "$IPERF_CONNECT_TO_IFACE" -u -b $1M -t $TEST_TIME --logfile "$RAMDISK/iperf_s2c_$1.txt"

    #Once the tests are over, stop the monitor
    kill -s 12 $monitor_pid
    if mv $RAMDISK/* $CUR_DIR/ ; then
        echo "All files copied to $CUR_DIR"
        echo
        echo
        parse_iperf "s2c" $1 || { echo "An error occured in parse_iperf function"; return 1; }
        return 0
    else
        echo "Copy of files failed, do it yourself."
        echo "iPerf results won't be in csv format and won't have an epoch time"
        echo "You can obtain the CSV file by looking at parse_iperf function, but you won't be able to get an epoch for it."
        echo "Files are located at $RAMDISK"
        echo
        echo
        return 1
    fi
}

function parse_iperf(){
    if [[ $1 == "c2s" ]]; then
        echo "Time (s);Interval (s);Transfer (MB);Bandwidth (Mb/s);Total Datagrams" > "$CUR_DIR/iperf_c2s_$2.csv"
        data=$(cat "$CUR_DIR/iperf_c2s_$2.txt" | head -n -6 | tail -n +4 | sed 's/\[  5\] //' | sed 's/ sec  /;/' | sed 's/ MBytes  /;/' | sed 's/ Mbits\/sec  /;/' | tr -s ' ' | tr -d ' ')
        for record in $data; do
            # From the record, we extract the time interval.
            # From the time interval, we take the start of the interval.
            # The number will be a floating point: Bash doesn't handle floating
            # point arithmetic, so we remove the . from the interval and make
            # the time as integer
            t=$(echo $record | cut -d ';' -f 1 | cut -d '-' -f 1 | cut -d '.' -f 1)
            let t+=$SAMPLING_START
            echo "$t;$record" >> "$CUR_DIR/iperf_c2s_$2.csv"
        done
        return 0
    elif [[ $1 == "s2c" ]]; then
        echo "Time (s);Interval (s);Transfer (MB);Bandwidth(Mb/s);Jitter(ms);Lost/Total Datagrams (%)" > "$CUR_DIR/iperf_s2c_$2.csv"
        data=$(cat "$CUR_DIR/iperf_s2c_$2.txt" | head -n -6 | tail -n +5 | sed 's/\[  5\] //' | sed 's/ sec  /;/' | sed 's/ MBytes  /;/' | sed 's/ Mbits\/sec  /;/' | sed 's/ ms  /;/' | tr -s ' ' | tr -d ' ')
        for record in $data; do
            # From the record, we extract the time interval.
            # From the time interval, we take the start of the interval.
            # The number will be a floating point: Bash doesn't handle floating
            # point arithmetic, so we remove the . from the interval and make
            # the time as integer
            t=$(echo $record | cut -d ';' -f 1 | cut -d '-' -f 1 | cut -d '.' -f 1)
            let t+=$SAMPLING_START
            echo "$t;$record" >> "$CUR_DIR/iperf_s2c_$2.csv"
        done
        return 0        
    else
        echo "parse_iperf failed! Probably you passed the wrong parameters"
        return 1
    fi
}

#If the arguments are not 4 print help and exit
if [[ "$#" -ne 4 ]]; then
    print_help "args"
    exit 1
else
    psw=$1          #Save the root password
    NTP_SERVER=$2   #Save the NTP server address

    #Check if ntpdate is installed, if not exit
    type ntpdate >/dev/null 2>&1 || { print_help "ntpdate"; exit 2;}
    #Check if the NTP server is valid and reachable, if not exit
    echo $psw | sudo -S ntpdate -u $NTP_SERVER > /dev/null 2>&1|| { echo "Cannot find NTP server $NTP_SERVER"; exit 3; }

    # If the monitor program is not found, print help and exit
    if [[ ! -e "$MONITOR_PROG" ]]; then
        print_help "monitor"
        exit 4
    fi

    #Check if iperf3 is installed, if not exit
    type iperf3 > /dev/null 2>&1 || { print_help "iperf"; exit 5;}
    iface=$3
    IPERF_BIND_IFACE=$(ip -f inet -o addr show $iface | cut -d\  -f 7 | cut -d/ -f 1)
    if [[ $IPERF_BIND_IFACE == "" ]]; then
        exit 6;
    fi

    IPERF_CONNECT_TO_IFACE=$4
    ping -w 3 $IPERF_CONNECT_TO_IFACE &> /dev/null || { echo "Server $IPERF_CONNECT_TO_IFACE is not reachable"; exit 7; }

    #Make the ramdisk
    make_ramdisk $psw || { echo "Ramdisk creation failed!"; exit 8; }
fi

#Run all the needed test with different load and direction. In case of errors, stop and exit
run_c2s_test 10 || exit 6
run_c2s_test 20 || exit 6
run_c2s_test 30 || exit 6
run_c2s_test 40 || exit 6
run_c2s_test 50 || exit 6
run_c2s_test 60 || exit 6
run_c2s_test 70 || exit 6
run_c2s_test 80 || exit 6
run_c2s_test 90 || exit 6
run_c2s_test 100 || exit 6

#run_s2c_test 10 || exit 6
#run_s2c_test 20 || exit 6
#run_s2c_test 30 || exit 6
#run_s2c_test 40 || exit 6
#run_s2c_test 50 || exit 6
#run_s2c_test 60 || exit 6
#run_s2c_test 70 || exit 6
#run_s2c_test 80 || exit 6
#run_s2c_test 90 || exit 6
#run_s2c_test 100 || exit 6