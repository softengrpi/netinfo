#!/usr/bin/env python3

'''
Quick roadmap of what I should do.
    1. Plot all the points
    2. Make a linear regression: order of the regression should be such
    that it minimizes the error between predicted value and actual
    value, i.e min(abs(predicted - actual))
    3. Obtain a formula of the regression with the coefficients.
'''

from pandas import read_csv
import numpy as np
import matplotlib.pyplot as plt

max_degree = 4
screen_dpi = 96  # Should be adjusted according to the screen DPI of your PC


def get_formula(coefficients):
    '''
        Coefficients are in a list ordered
        from the higher-order to the
        lower-order.

        Returns a string representing the formula
    '''
    formula = str()
    degree = len(coefficients) - 1
    for c in coefficients:
        if degree == 0:
            if c >= 0:
                formula += "+{0:.4f}".format(c)
            else:
                formula += "{0:.4f}".format(c)
        elif degree == 1:
            if c >= 0:
                formula += "+{0:.4f}$r$ ".format(c)
            else:
                formula += "{0:.4f}$r$ ".format(c)
        else:
            if c >= 0:
                formula += "+{0:.4f}$r^{1}$ ".format(c, degree)
            else:
                formula += "{0:.4f}$r^{1}$ ".format(c, degree)
        degree -= 1
    return "P($r$) = " + formula


def compute_regressions(bandwidth, power, max_reg, line_setup, plt):
    '''
        1. Computes regressions up to max_reg.
        2. Plot the regression

        Returns a string with information about the regression
    '''
    p_data = str()
    for degree in range(1, max_reg + 1):
        p = np.poly1d(np.polyfit(bandwidth, power, degree))
        p_predict = np.polyval(p, bandwidth)
        p_error = np.mean((p_predict - power)**2)
        p_rmse = np.sqrt(p_error)
        p_data += "{}\nRMSE: {num:.{digits}f} mW\n\n".format(get_formula(p.c), num=p_rmse * 1000, digits=0)
        print(p)
        plt.plot(bandwidth, p_predict,
                 label="Regression order {}".format(degree),
                 color=line_setup[degree][0],
                 linestyle=line_setup[degree][1],
                 linewidth=line_setup[degree][2])
    return p_data

data_frame_c2s = read_csv("net_c2s_bdw_vs_pwr.csv", sep=";", header=0, names=["bandwidth", "power"]).sort_values('bandwidth')
bdws_c2s = data_frame_c2s.get('bandwidth')
powers_c2s = data_frame_c2s.get('power')

data_frame_s2c = read_csv("net_s2c_bdw_vs_pwr.csv", sep=";", header=0, names=["bandwidth", "power"]).sort_values('bandwidth')
bdws_s2c = data_frame_s2c.get('bandwidth')
powers_s2c = data_frame_s2c.get('power')

line_setup = {
    #   color, line style, line width
    #   Red
    1: ("#FF2828", "-", 2),
    #   Light Blue
    2: ("#5EC2FD", "--", 3),
    #   Gold
    3: ("#C0A753", "-.", 3),
    #   Dark Green
    4: ("#039C1F", "-", 2)}

# Setup the plot
plt.subplot(211)
plt.title("Power consumption vs. Ethernet Upload Bandwidth")
plt.xlabel("Upload Bandwidth (Mb/s)")
plt.ylabel("Power Consumption (W)")
plt.plot(bdws_c2s, powers_c2s, '.', label="C2S Collected Data", color="#B7B7B7")
plt.xticks(np.arange(0, 110, 10), np.arange(0, 110, 10))
plt.xlim((0, 105))

poly_data = compute_regressions(bdws_c2s, powers_c2s, max_degree, line_setup, plt)

plt.text(0.02, 0.95, poly_data, transform=plt.gca().transAxes, va="top", ha="left", fontsize=13)
plt.legend(loc="lower right")


# Setup the plot
plt.subplot(212)
plt.title("Power consumption vs. Ethernet Download Bandwidth")
plt.xlabel("Download Bandwidth (Mb/s)")
plt.ylabel("Power Consumption (W)")
plt.plot(bdws_s2c, powers_s2c, '.', label="S2C Collected Data", color="#B7B7B7")
plt.xticks(np.arange(0, 110, 10), np.arange(0, 110, 10))
plt.xlim((0, 105))

poly_data = compute_regressions(bdws_s2c, powers_s2c, max_degree, line_setup, plt)

plt.text(0.02, 0.95, poly_data, transform=plt.gca().transAxes, va="top", ha="left", fontsize=13)
plt.legend(loc="lower right")

cur_fig = plt.gcf()

plt.show()
cur_fig.savefig('plot.png', dpi=screen_dpi)
