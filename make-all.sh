#!/usr/bin/env bash

MONITOR_SRC="monitor.c"
ROOT_DIR=$(pwd)

#Compile the monitor
gcc -O3 -o `basename $MONITOR_SRC .c`.out $MONITOR_SRC