#!/usr/bin/env python3
#
# The goal of this script is to manipulate the raw data collected through the
# DAQ and the /proc/net/dev device and produce a table of Network usages vs. power consumptions.
# The script will do the following:
#   1. Convert the 1904 epoch of the DAQ measures into 1970 epoch so that data from DAQ
#   can be compared to the net usage fetched from the /proc/net/dev linux device
#   2. Remove the measures from the DAQ file that do not match any of the epochs from
#   an input CPU usage file
#   3. Finally create a unique file containing all the cpu usages vs. the power
#   consumptions.
# Once all this is done, we are ready to plot everything with R.

import os
import sys
from collections import OrderedDict
from math import floor

if len(sys.argv) < 2:
    print("Please supply c2s or s2c if you want to fetch data from, respectively, client to server or server to client")
    sys.exit(1)

if sys.argv[1] != "c2s" and sys.argv[1] != "s2c":
    print("Unknown argument. Possible argument is c2s or s2c")
    sys.exit(1)

cur_dir = os.getcwd()
epoch_diff = 2082844800

input_DAQ_file = cur_dir + "/acquired logs/ethernet_" + sys.argv[1] + "_22-02-17.txt"

net_data = OrderedDict()
net_data[10] = dict()
net_data[20] = dict()
net_data[30] = dict()
net_data[40] = dict()
net_data[50] = dict()
net_data[60] = dict()
net_data[70] = dict()
net_data[80] = dict()
net_data[90] = dict()
net_data[100] = dict()


output_final_file = cur_dir + "/acquired logs/net_" + sys.argv[1] + "_bdw_vs_pwr.csv"
# List of tuples. Tuple contains (time, power)
DAQ_data = list()


def read_bdw_files():
    bdw = 10  # Start index
    input_bdw_files = OrderedDict()
    input_bdw_files["10"] = cur_dir + "/acquired logs/net_data_dump_" + sys.argv[1] + "_10.csv"
    input_bdw_files["20"] = cur_dir + "/acquired logs/net_data_dump_" + sys.argv[1] + "_20.csv"
    input_bdw_files["30"] = cur_dir + "/acquired logs/net_data_dump_" + sys.argv[1] + "_30.csv"
    input_bdw_files["40"] = cur_dir + "/acquired logs/net_data_dump_" + sys.argv[1] + "_40.csv"
    input_bdw_files["50"] = cur_dir + "/acquired logs/net_data_dump_" + sys.argv[1] + "_50.csv"
    input_bdw_files["60"] = cur_dir + "/acquired logs/net_data_dump_" + sys.argv[1] + "_60.csv"
    input_bdw_files["70"] = cur_dir + "/acquired logs/net_data_dump_" + sys.argv[1] + "_70.csv"
    input_bdw_files["80"] = cur_dir + "/acquired logs/net_data_dump_" + sys.argv[1] + "_80.csv"
    input_bdw_files["90"] = cur_dir + "/acquired logs/net_data_dump_" + sys.argv[1] + "_90.csv"
    input_bdw_files["100"] = cur_dir + "/acquired logs/net_data_dump_" + sys.argv[1] + "_100.csv"
    # Loop thorugh each file and open it
    for path in input_bdw_files.values():
        with open(path, 'r') as fd_usage_file:
            # Skip the first 3 lines, i.e, header and first value that is null
            for skip in range(0, 3):
                next(fd_usage_file)
            # Loop through the lines of the file and get the time and usage value
            for line in fd_usage_file:
                values = line.split(";")
                time = int(values[0])
                if sys.argv[1] == "c2s":
                    # Convert measure from Bytes/s to Mbits/s
                    bdw_val = (float(values[3]) * 8) / (1024**2)
                else:
                    # Convert measure from Bytes/s to Mbits/s
                    bdw_val = (float(values[2]) * 8) / (1024**2)
                net_data[bdw][time] = bdw_val
        bdw += 10


def get_min_max_epoch_list():
    # Return a list of tuples containing the minimum and maximum epoch of each CPU %
    epoch_list = list()
    for bdw_key in range(10, 101, 10):
        epoch_list.append((min(list(net_data[bdw_key].keys())),
                           max(list(net_data[bdw_key].keys()))))
    return epoch_list


def read_DAQ_file():
    index_epoch_list = 0
    # is_in_range: Used to tell if we ever stepped into an epoch range inside
    # the DAQ file.
    # We use it also to understand when we step out of the range and know that
    # a new NET bandwidth measure starts
    is_in_range = False
    with open(input_DAQ_file, 'r') as fd_DAQ_in:
        # Skip the header
        next(fd_DAQ_in)
        for line in fd_DAQ_in:
            values = line.split(";")
            # Convert EPOCH 1904 -> EPOCH 1970
            time = float(values[0].replace(",", "."))
            time -= epoch_diff
            # If time is in the range of the min and max epoch.
            # Max epoch +1 because time is float, so we must consider also x.99..99 upper bound
            if time >= epoch_list[index_epoch_list][0] and time < (epoch_list[index_epoch_list][1] + 1):
                power = float(values[1].replace(",", ".")) * float(values[2].replace(",", "."))
                DAQ_data.append((time, power))
                is_in_range = True  # Trigger the flag, so we know we stepped into an epoch interval
            elif is_in_range:
                is_in_range = False
                index_epoch_list += 1
                if index_epoch_list >= len(epoch_list):
                    break


def average_DAQ_data():
    cur_floor = None
    count = 0
    output = dict()
    for data in DAQ_data:
        # data[0] = epoch
        # data[1] = power
        if not cur_floor:
            cur_floor = floor(data[0])
            output[cur_floor] = 0.0
        if cur_floor != floor(data[0]):
            output[cur_floor] /= count
            cur_floor = floor(data[0])
            count = 0
            output[cur_floor] = 0.0
        output[cur_floor] += data[1]
        count += 1
    output[cur_floor] /= count  # Divide also the last set of data
    return output


def make_output_file():
    with open(output_final_file, 'w') as fd_out:
        fd_out.write("Bandwidth (Mb/s); Power (W)\n")
        for epoch_and_bdw in net_data.values():
            for epoch, bdw in epoch_and_bdw.items():
                avg_pwr = DAQ_data_averaged[epoch]
                fd_out.write("{};{}\n".format(bdw, avg_pwr))


read_bdw_files()
epoch_list = get_min_max_epoch_list()
read_DAQ_file()
DAQ_data_averaged = average_DAQ_data()
make_output_file()
