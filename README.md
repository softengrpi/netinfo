#Raspberry Pi Network monitor
This small script tracks the bytes transferred and received by parsing the output of `/proc/net/dev` .

The results are written on a CSV file for easy view and formatting.

#Usage

    netinfo <root-psw> <seconds> <iface>

Where:

  + `root-psw` is the superuser password, which is required to create a *ramdisk* mount point on `/tmp`
  + `seconds` is the amount of time we want to sample.
  + `iface` is the name of the network interface we want to monitor
  