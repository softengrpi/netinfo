#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <signal.h>

#define STR_LENGHT 80

static const char APP_NAME[] = "netinfo";

//Prototypes
void print_help(void);
void sample_net_data();
void save_data();


/**
 * These global variables are here because signal function doesn't allow passing
 * parameters to signal handler.
 */
unsigned long sample_time = -1;
char ramdisk_path[STR_LENGHT] = {0};
char interface[STR_LENGHT] = {0};
char direction[4] = {0};
FILE* fp_pipe;
FILE* fp_file;
unsigned long load = -1;

int main(int argc, char* argv[]) {

    if(argc != 5 ){
        print_help();
        return -1;
    }
    //Save the load amount
    if((load = strtoul(argv[3], NULL, 10)) == ULONG_MAX || load == 0 ){
        printf("Cannot convert %s to unsigned long\nEither the value is out of range or is not a valida base-10 integer", argv[3]);
        return -1;
    }

    //Save the path to ramdisk
    snprintf(ramdisk_path, STR_LENGHT, "%s", argv[1]);

    //Save the name of the interface
    snprintf(interface, STR_LENGHT, "%s", argv[2]);

    //Save the direction of the transfer
    snprintf(direction, 4, "%s", argv[4]);

    signal(SIGUSR1, sample_net_data);
    printf("Waiting for start signal..\n");
    pause();

    return 0;
}

/**
 * Prints to stdin a help message
 */
void print_help(){
    printf("error: Not enough parameters\n\nUsage: %s <ramdisk> <iface> <load> c2s|s2c\n", APP_NAME);
    printf("\t- <ramdisk>: path to a ramdisk\n");
    printf("\t- <iface>: Network interface to sample\n");
    printf("\t- <load>: The load we are using\n");
    printf("\t- c2s|s2c: The direction of the transfer, either from client to server (c2s) OR from server to client (s2c)\n");
}

/**
 * Reads from /proc/net/dev the amount of transmitted/received bytes. 
 * Computes the current bandwith (transmitted/received) as:
 *            (Bytes_tx_t - Bytes_tx_t_1)
 *     r(t) = ----------------------------
 *                    (t - t_1)
 * r(t) is then saved onto a file in csv format.
 * 
 */
void sample_net_data(){
    //Stop reacting to SIGUSR1
    signal(SIGUSR1, SIG_IGN);
    printf("Start sampling..\n");

    char net_interface[STR_LENGHT] = {0};
    char data_filename[STR_LENGHT*2] = {0};
    unsigned long iterations=0;
    unsigned long b_tx_old = 0;
    unsigned long b_rx_old = 0;
    unsigned long t_old = 0;

    snprintf(data_filename, strlen(ramdisk_path)+STR_LENGHT, "%s/net_data_dump_%s_%lu.csv", ramdisk_path, direction, load);
    if((fp_file=fopen(data_filename, "w")) == NULL){
        printf("error: Cannot create file %s to save dumps\n", data_filename);
        return;
    }
    while(1){
        /**
         * We ignore SIGUSR2 at the beginning of each iteration so that we can
         * complete a whole cycle without being interrupted.
         * This helps in keeping file pointers (fp_pipe mainly) in a consistent
         * state.
         */
        signal(SIGUSR2, SIG_IGN);
        unsigned long b_tx = 0;
        unsigned long b_rx = 0;
        unsigned long t = time(NULL);
        int is_EOF;
        if ((fp_pipe=popen("cat /proc/net/dev", "r")) == NULL) {
            printf("error: Cannot open pipe for 'cat /proc/net/dev' command\n");
            return;
        }
        //Discard first and second lines of output
        fscanf(fp_pipe, "%*[^\n]\n%*[^\n]\n", NULL, NULL);
        /**
         * Keep reading from the pipe until we reach the interface we want to sample
         * or we reach the end of file.
         * We compare network interface name up until strlen(interface)-1 characters
         * because /proc/net/dev returns the interface name followed by a ':' . 
         */
        do {
            is_EOF=fscanf(fp_pipe, "%s %lu %*lu %*lu %*lu %*lu %*lu %*lu %*lu %lu", net_interface, &b_rx, &b_tx);
        } while(strncmp(net_interface, interface, strlen(interface)-1) != 0 && is_EOF != EOF);
        if (iterations == 0 && is_EOF == EOF){
            printf("error: Cannot find '%s' network interface\nQuitting...\n", interface);
            return;
        }
        else if (iterations == 0){ //If it's the first iteration, we cannot compute the utilization as we have no previous data
            fprintf(fp_file, "Time (s);Iteration;Rx (B/s);Tx (B/s)\n\n");
            fprintf(fp_file, "%lu;%lu;0.0;0.0\n", time(NULL), iterations);
        }
        else {
            double bwd_rx = (double)(b_rx - b_rx_old)/(double)(t - t_old);
            double bwd_tx = (double)(b_tx - b_tx_old)/(double)(t - t_old);
            fprintf(fp_file, "%lu;%lu;%f;%f\n", time(NULL), iterations, bwd_rx, bwd_tx);
        }
        fclose(fp_pipe);
        /**
         * Once we close fp_pipe we know we have a complete cycle.
         * This is the only window in which we can be stopped.
         */
        signal(SIGUSR2, save_data);
        iterations++;
        t_old = t;
        b_rx_old = b_rx;
        b_tx_old = b_tx;
        sleep(1);
    }
}

void save_data(){
    signal(SIGUSR2, SIG_IGN);
    fclose(fp_file);
    raise(SIGTERM);
}